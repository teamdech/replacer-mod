package dechmods.blockreplacer;

import cofh.api.energy.IEnergyContainerItem;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.EnumMovingObjectType;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

import java.util.List;

public class ItemBlockReplacer extends ItemSword implements IEnergyContainerItem {

    protected int capacity;
    protected int maxReceive;

    public ItemBlockReplacer(int itemID, int capacity, int maxTransfer) {
        super(itemID - 256, EnumToolMaterial.WOOD);
        setCreativeTab(CreativeTabs.tabTools);
        this.capacity = capacity;
        maxReceive = maxTransfer;
        setMaxDamage(102);
    }

    @Override
    public void registerIcons(IconRegister iconRegister) {
        itemIcon = iconRegister.registerIcon("blockreplacer:blockReplacer");
    }

    @Override
    public boolean onEntitySwing(EntityLivingBase entity, ItemStack stack) {
        if (entity.worldObj.isRemote || !(entity instanceof EntityPlayer)) return false;
        MovingObjectPosition pos = VectorUtils.getMovingObjectPositionFromPlayer(entity.worldObj, (EntityPlayer) entity, false);
        if (pos != null && pos.typeOfHit == EnumMovingObjectType.TILE) {
            int x = pos.blockX, y = pos.blockY, z = pos.blockZ;
            ReplaceUtils.replaceBlockAt((EntityPlayer) entity, x, y, z, entity.worldObj, stack, ModBlockReplacer.energyCost, pos.sideHit);
        }
        return false;
    }

    @Override
    public boolean onItemUseFirst(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ) {
        if (world.isRemote) return false;

        // Below runs only on server

        if (player.isSneaking()) {
            if (world.getBlockTileEntity(x, y, z) == null && world.isBlockFullCube(x, y, z)) {
                getTagCompound(stack).setInteger("blockID", world.getBlockId(x, y, z));
                getTagCompound(stack).setInteger("blockMeta", world.getBlockMetadata(x, y, z));
            }
        } else {
            MovingObjectPosition pos = VectorUtils.getMovingObjectPositionFromPlayer(world, player, false);
            if (pos != null && pos.typeOfHit == EnumMovingObjectType.TILE) {
                ModBlockReplacer.tickHandler.addReplacer(new Replacer(pos.blockX, pos.blockY, pos.blockZ, pos.sideHit, stack, player, world));
            }
        }

        return false;
    }

    @Override
    public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
        return stack;
    }

    @Override
    public void addInformation(ItemStack itemStack, EntityPlayer entityPlayer, List list, boolean bool) {
        list.add("Replacing with " + EnumChatFormatting.AQUA + Block.blocksList[getTagCompound(itemStack).getInteger("blockID")].getLocalizedName());
        list.add((getEnergyStored(itemStack) / 1F * (getMaxEnergyStored(itemStack)) > 0.2F ? EnumChatFormatting.DARK_GREEN : EnumChatFormatting.DARK_RED) + "" + getEnergyStored(itemStack) + " RF / " + getMaxEnergyStored(itemStack) + " RF");
    }

    private NBTTagCompound getTagCompound(ItemStack stack) {
        if (stack.stackTagCompound == null) stack.stackTagCompound = new NBTTagCompound();
        if (stack.stackTagCompound.getInteger("blockID") == 0) stack.stackTagCompound.setInteger("blockID", 1);
        if (!stack.stackTagCompound.hasKey("Energy")) stack.stackTagCompound.setInteger("Energy", 0);

        return stack.stackTagCompound;
    }

    // Visual crap

    @Override
    public boolean isFull3D() {
        return true;
    }

    @Override
    public boolean onBlockStartBreak(ItemStack stack, int x, int y, int z, EntityPlayer player) {
        return true;
    }

    @Override
    public float getStrVsBlock(ItemStack stack, Block block) {
        return 0;
    }

    @Override
    public boolean canHarvestBlock(Block block) {
        return true;
    }

    @Override
    public boolean onBlockDestroyed(ItemStack stack, World world, int x, int y, int z, int side, EntityLivingBase player) {
        return true;
    }

    @Override
    public EnumAction getItemUseAction(ItemStack stack) {
        return EnumAction.none;
    }

    @Override
    public void getSubItems(int itemID, CreativeTabs tab, List items) {
        ItemStack r = new ItemStack(itemID, 1, 0);
        getTagCompound(r).setInteger("Energy", 100000);
        items.add(r);
    }

    @Override
    public boolean hitEntity(ItemStack stack, EntityLivingBase entity, EntityLivingBase player) {
        return true;
    }

    @Override
    public int getDamage(ItemStack stack) {
        setDamage(stack, 101 - (int) ((float) getTagCompound(stack).getInteger("Energy") * 100F / 100000F));
        return 101 - (int) ((float) getTagCompound(stack).getInteger("Energy") * 100F / 100000F);
    }

    @Override
    public int getMaxDamage(ItemStack stack) {
        return 102;
    }

    // Energy handling

    @Override
    public int receiveEnergy(ItemStack container, int maxReceive, boolean simulate) {

        if (container.stackTagCompound == null) {
            container.stackTagCompound = new NBTTagCompound();
        }
        int energy = container.stackTagCompound.getInteger("Energy");
        int energyReceived = Math.min(capacity - energy, Math.min(this.maxReceive, maxReceive));

        if (!simulate) {
            energy += energyReceived;
            container.stackTagCompound.setInteger("Energy", energy);
        }
        return energyReceived;
    }

    @Override
    public int extractEnergy(ItemStack container, int maxExtract, boolean simulate) {
        return 0;
    }

    @Override
    public int getEnergyStored(ItemStack container) {
        if (container.stackTagCompound == null || !container.stackTagCompound.hasKey("Energy")) {
            return 0;
        }
        return container.stackTagCompound.getInteger("Energy");
    }

    @Override
    public int getMaxEnergyStored(ItemStack container) {
        return capacity;
    }
}
