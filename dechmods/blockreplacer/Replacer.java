package dechmods.blockreplacer;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class Replacer {
    public static final int[] delta1 = new int[] { 0, 0, 1, 0, -1, 0, 1, 2, 1, 0, -1, -2, -1, 0, 1, 2, 3, 2, 1, 0, -1, -2, -3, -2, -1, 0, 1, 2, 3, 4, 3, 2, 1, 0, -1, -2, -3, -4, -3, -2, -1 };
    public static final int[] delta2 = new int[] { 0, 1, 0, -1, 0, 2, 1, 0, -1, -2, -1, 0, 1, 3, 2, 1, 0, -1, -2, -3, -2, -1, 0, 1, 2, 4, 3, 2, 1, 0, -1, -2, -3, -4, -3, -2, -1, 0, 1, 2, 3 };
    public static final int[] base1 = new int[] { -1, 0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 7, 7, 7, 9, 9, 9, 11, 11, 11, 5, 13, 13, 14, 16, 16, 16, 17, 19, 19, 19, 20, 22, 22, 22, 23, 13 };
    public static final int[] base2 = new int[] { -1, 0, 0, 0, 0, 1, 2, 2, 3, 3, 4, 4, 1, 5, 6, 6, 7, 8, 8, 9, 10, 10, 11, 12, 12, 13, 14, 15, 15, 16, 17, 18, 18, 19, 20, 21, 21, 22, 23, 24, 24 };

    private int x, y, z, side;
    private boolean doX, doY, doZ;
    private ItemStack stack;
    private EntityPlayer player;
    private World world;
    private int block, meta;
    private int i;
    private boolean[] blocks = new boolean[41];

    public Replacer(int x, int y, int z, int side, ItemStack stack, EntityPlayer player, World world) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.side = side;
        this.stack = stack;
        this.player = player;
        this.world = world;

        block = world.getBlockId(x, y, z);
        meta = world.getBlockMetadata(x, y, z);

        doX = side < 4;
        doY = side > 1;
        doZ = side < 2 || side > 3;
    }

    public boolean run() {
        if (i > 40)
            return false;

        int nX = x + (doX ? delta1[i] : 0);
        int nY = y + (doY ? (doX ? delta2[i] : delta1[i]) : 0);
        int nZ = z + (doZ ? delta2[i] : 0);

        if (base1[i] == -1 || blocks[base1[i]] == true || blocks[base1[i]] == true) {
            int tID = world.getBlockId(nX, nY, nZ), tMeta = world.getBlockMetadata(nX, nY, nZ);
            if (ReplaceUtils.canReplace(player, nX, nY, nZ, world, stack) && tID == block && tMeta == meta && !world.isBlockFullCube(nX + (side == 5 ? 1 : (side == 4 ? -1 : 0)), nY + (side == 1 ? 1 : (side == 0 ? -1 : 0)), nZ + (side == 3 ? 1 : (side == 2 ? -1 : 0)))) {
                blocks[i] = true;
                ReplaceUtils.replaceBlockAt(player, nX, nY, nZ, world, stack, ModBlockReplacer.energyCost, side);
            }
        }

        i++;

        if (blocks[i - 1] == false) {
            run();
        }

        return true;
    }
}
