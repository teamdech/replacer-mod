package dechmods.blockreplacer;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.Configuration;

@Mod(modid = "blockReplacer", name = "Block Replacer", version = "1.0")
@NetworkMod(clientSideRequired = true, serverSideRequired = false)
public class ModBlockReplacer {
    public static int energyCost = 100, replacerID = 1337;
    public static Item blockReplacer;
    public static ReplaceTickHandler tickHandler;

    @EventHandler
    public void preLoad(FMLPreInitializationEvent event) {
        Configuration config = new Configuration(event.getSuggestedConfigurationFile());

        config.load();

        energyCost = config.get("general", "EnergyPerBlockReplaced", 100).getInt(100);
        replacerID = config.get("IDs", "ReplacerItemID", 1337).getInt(1337);
    }

    @EventHandler
    public void load(FMLInitializationEvent e) {
        blockReplacer = new ItemBlockReplacer(replacerID, 100000, 10000).setUnlocalizedName("blockReplacer");
        GameRegistry.registerItem(blockReplacer, "blockReplacer");
        LanguageRegistry.addName(blockReplacer, "Staff of Block Replacing");
        GameRegistry.addRecipe(new ItemStack(blockReplacer, 1, 0), "E  ", " S ", "  R", 'E', Item.eyeOfEnder, 'S', Item.stick, 'R', Block.blockRedstone);
        NetworkRegistry.instance().registerChannel(new NetHandler(), "blockreplacer");
        
        tickHandler = new ReplaceTickHandler();
        TickRegistry.registerTickHandler(tickHandler, Side.SERVER);
    }
}
