package dechmods.blockreplacer;

import net.minecraft.block.Block;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

import java.util.ArrayList;

public class ReplaceUtils {

    /**
     * Checks if the player is able to replace the specified block
     *
     * @param player The player to check
     * @param x      The x coordinate of the block
     * @param y      The y coordinate of the block
     * @param z      The z coordinate of the block
     * @param world  The world in which the block is in
     * @param rStack The {@link net.minecraft.item.ItemStack} of the {@link ItemBlockReplacer}
     * @return if the player can replace the specified block
     */
    public static boolean canReplace(EntityPlayer player, int x, int y, int z, World world, ItemStack rStack) {
        NBTTagCompound c = rStack.getTagCompound();

        int rID = c.getInteger("blockID"), rMeta = c.getInteger("blockMeta");
        int ID = world.getBlockId(x, y, z), meta = world.getBlockMetadata(x, y, z);

        return !world.isAirBlock(x, y, z) && world.isBlockFullCube(x, y, z) && playerHasItem(player, rID, rMeta) && (rID != ID || rMeta != meta) && (c.getInteger("Energy") >= ModBlockReplacer.energyCost || player.capabilities.isCreativeMode);
    }

    /**
     * Replaces a block at a specific position based on the <b>stackTagCompound</b>
     *
     * @param player     The player to check <i>(uses {@link #canReplace(net.minecraft.entity.player.EntityPlayer, int, int, int, net.minecraft.world.World, net.minecraft.item.ItemStack)})</i>
     * @param x          The x coordinate of the block to replace
     * @param y          The y coordinate of the block to replace
     * @param z          The z coordinate of the block to replace
     * @param world      The world in which the block is in
     * @param rStack     The The {@link net.minecraft.item.ItemStack} of the {@link ItemBlockReplacer}
     * @param energyCost The energycost of the action
     * @return if replacing the block succeeded
     */
    public static boolean replaceBlockAt(EntityPlayer player, int x, int y, int z, World world, ItemStack rStack, int energyCost, int side) {
        if (!canReplace(player, x, y, z, world, rStack)) return false;

        int rID = rStack.getTagCompound().getInteger("blockID"), rMeta = rStack.getTagCompound().getInteger("blockMeta");
        int ID = world.getBlockId(x, y, z), meta = world.getBlockMetadata(x, y, z);

        // Extract energy
        if (!(player.capabilities.isCreativeMode || extractEnergy(rStack, energyCost))) return false;

        // Remove block from inventory
        if (!(player.capabilities.isCreativeMode || removeItem(player.inventory, rID, rMeta, 1, -1))) return false;

        // Replace block in world
        if (!player.capabilities.isCreativeMode) {
            Block b = Block.blocksList[ID];
            EntityItem item = new EntityItem(world, player.posX, player.posY, player.posZ, new ItemStack(b.idDropped(meta, world.rand, 0), b.quantityDropped(meta, 0, world.rand), b.damageDropped(meta)));
            item.delayBeforeCanPickup = 0;
            item.lifespan = 300;
            world.spawnEntityInWorld(item);
        }

        NetHandler.replaceBlock(world, x, y, z, side);
        world.setBlock(x, y, z, rID, rMeta, 3);

        return true;
    }

    /**
     * Checks if the player has a specific item <b>or is in creative mode</b>
     * <p/>
     * Equivalent to {@link #playerHasItem(net.minecraft.entity.player.EntityPlayer, int, int, boolean)} with the last parameter <code>false</code>
     *
     * @return If the player has the specified item
     * @see #playerHasItem(net.minecraft.entity.player.EntityPlayer, int, int, boolean)
     */
    public static boolean playerHasItem(EntityPlayer player, int id, int meta) {
        return playerHasItem(player, id, meta, false);
    }

    /**
     * Checks if a player has the specified item
     *
     * @param player         The player to check for the item
     * @param id             The itemID the player should have
     * @param meta           The metadata the item should have <u><b>or -1 to not check metadata</b></u>
     * @param ignoreCreative If the creative mode should be ignored or not <i>Example: false -> if the player is in creative mode, return true</i>
     * @return If the player has the specified item
     */
    public static boolean playerHasItem(EntityPlayer player, int id, int meta, boolean ignoreCreative) {
        if (!ignoreCreative && player.capabilities.isCreativeMode) return true;
        InventoryPlayer invPlayer = player.inventory;

        for (int i = 0; i < invPlayer.getSizeInventory(); i++) {
            ItemStack stack = invPlayer.getStackInSlot(i);
            if (stack != null && stack.itemID == id && (meta == -1 || stack.getItemDamage() == meta)) return true;
        }

        return false;
    }

    /**
     * Removes items from a players inventory
     *
     * @param inventoryPlayer The inventory to remove items from
     * @param itemID          The itemID to remove
     * @param meta            The metadata the item should have <u><b>or -1 to not check metadata</b></u>
     * @param count           The amount of items to remove
     * @param slot            The slot the item can be found in <u><b>or -1 to find the slot</b></u>
     * @return If the item(s) were successfully removed
     */
    public static boolean removeItem(InventoryPlayer inventoryPlayer, int itemID, int meta, int count, int slot) {
        ItemStack stack;
        if (slot == -1) {
            slot = findSlot(inventoryPlayer, itemID, meta);
            if (slot == -1) return false;
            stack = inventoryPlayer.getStackInSlot(slot);
        } else {
            stack = inventoryPlayer.getStackInSlot(slot);
            if (stack == null || stack.getItem() == null || stack.getItem().itemID != itemID || (meta != -1 && stack.getItemDamage() != meta))
                return false;
        }
        if (stack.stackSize < count) return false;

        stack.stackSize -= count;
        inventoryPlayer.setInventorySlotContents(slot, stack.stackSize == 0 ? null : stack);
        inventoryPlayer.onInventoryChanged();
        return true;
    }

    /**
     * Finds a slot which contains the specified item
     *
     * @param inventoryPlayer The inventory to search in
     * @param itemID          The itemID to find
     * @param meta            The metadata the item should have <u><b>or -1 to not check metadata</b></u>
     * @return The slot where the item was found <b>or -1 if the item could not be found</b>
     */
    public static int findSlot(InventoryPlayer inventoryPlayer, int itemID, int meta) {
        for (int i = 0; i < inventoryPlayer.getSizeInventory(); i++) {
            ItemStack stack = inventoryPlayer.getStackInSlot(i);
            if (stack == null) continue;
            Item item = stack.getItem();
            if (item == null) continue;

            if (item.itemID == itemID && (meta == -1 || stack.getItemDamage() == meta)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Removes energy from an itemstack
     *
     * @param stack           The itemstack to remove energy from
     * @param energyToExtract The amount of energy to extract
     * @return if removing the energy succeeded
     */
    public static boolean extractEnergy(ItemStack stack, int energyToExtract) {
        int energy = stack.stackTagCompound.getInteger("Energy");
        if (energy < energyToExtract) return false;

        energy -= energyToExtract;
        stack.stackTagCompound.setInteger("Energy", energy);

        return true;
    }
}
