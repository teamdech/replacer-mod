package dechmods.blockreplacer;

import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.common.network.Player;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.world.World;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;

public class NetHandler implements IPacketHandler {

    @Override
    public void onPacketData(INetworkManager manager, Packet250CustomPayload packet, Player player) {
        if (packet.channel.equals("blockreplacer")) {
            try {
                DataInputStream dis = new DataInputStream(new ByteArrayInputStream(packet.data));
                short subchannel = dis.readShort();
                if (subchannel == 0) {
                    EntityPlayer ePlayer = (EntityPlayer) player;
                    int x = dis.readInt(), y = dis.readInt(), z = dis.readInt(), side = dis.readInt();

                    if (ePlayer.worldObj.getBlockId(x, y, z) != 0 && Block.blocksList[ePlayer.worldObj.getBlockId(x, y, z)] != null) {
                        ePlayer.worldObj.playSound(x, y, z, Block.blocksList[ePlayer.worldObj.getBlockId(x, y, z)].stepSound.getBreakSound(), Block.blocksList[ePlayer.worldObj.getBlockId(x, y, z)].stepSound.stepSoundVolume, Block.blocksList[ePlayer.worldObj.getBlockId(x, y, z)].stepSound.stepSoundPitch - 0.2F, false);
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    public static void replaceBlock(World world, int x, int y, int z, int side) {
        ByteArrayOutputStream outputBytes = new ByteArrayOutputStream();
        DataOutputStream outputStream = new DataOutputStream(outputBytes);

        try {
            outputStream.writeShort(0);
            outputStream.writeInt(x);
            outputStream.writeInt(y);
            outputStream.writeInt(z);
            outputStream.writeInt(side);
        } catch (Exception e) {
        }

        PacketDispatcher.sendPacketToAllAround(x, y, z, 24, world.provider.dimensionId, new Packet250CustomPayload("blockreplacer", outputBytes.toByteArray()));
    }
}
