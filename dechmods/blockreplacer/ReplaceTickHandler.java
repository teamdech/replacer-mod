package dechmods.blockreplacer;

import java.util.ArrayList;
import java.util.EnumSet;

import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.TickType;

public class ReplaceTickHandler implements ITickHandler {

    private boolean hasReplacers = false;
    private ArrayList<Replacer> replacers = new ArrayList<Replacer>();

    @Override
    public void tickStart(EnumSet<TickType> type, Object... tickData) {
        if (hasReplacers) {
            for (int i = 0; i < replacers.size(); i++)
                if (!replacers.get(i).run())
                    replacers.remove(i);
            if (replacers.size() == 0)
                hasReplacers = false;
        }
    }

    public void addReplacer(Replacer replacer) {
        replacers.add(replacer);
        hasReplacers = true;
    }

    @Override
    public void tickEnd(EnumSet<TickType> type, Object... tickData) {
    }

    @Override
    public EnumSet<TickType> ticks() {
        return EnumSet.of(TickType.WORLD);
    }

    @Override
    public String getLabel() {
        return "replacetickhandler";
    }
}
