package dechmods.blockreplacer;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ReplaceBlocksThread implements Runnable {

    private ItemStack rStack;
    public static final int radius = 4, redstoneFluxPerBlock = 100;
    public static final int[] delta1 = new int[]{0, 0, 1, 0, -1, 0, 1, 2, 1, 0, -1, -2, -1, 0, 1, 2, 3, 2, 1, 0, -1, -2, -3, -2, -1, 0, 1, 2, 3, 4, 3, 2, 1, 0, -1, -2, -3, -4, -3, -2, -1};
    public static final int[] delta2 = new int[]{0, 1, 0, -1, 0, 2, 1, 0, -1, -2, -1, 0, 1, 3, 2, 1, 0, -1, -2, -3, -2, -1, 0, 1, 2, 4, 3, 2, 1, 0, -1, -2, -3, -4, -3, -2, -1, 0, 1, 2, 3};
    public static final int[] req1 = new int[]{-1, 0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 7, 7, 7, 9, 9, 9, 11, 11, 11, 5, 13, 13, 14, 16, 16, 16, 17, 19, 19, 19, 20, 22, 22, 22, 23, 13};
    public static final int[] req2 = new int[]{-1, 0, 0, 0, 0, 1, 2, 2, 3, 3, 4, 4, 1, 5, 6, 6, 7, 8, 8, 9, 10, 10, 11, 12, 12, 13, 14, 15, 15, 16, 17, 18, 18, 19, 20, 21, 21, 22, 23, 24, 24};
    private int blockSide, x, y, z;
    private EntityPlayer player;
    private World world;
    private int block;
    private int meta;

    public ReplaceBlocksThread(ItemBlockReplacer replacer, ItemStack rStack, int side, int x, int y, int z, EntityPlayer player, World world) {
        this.rStack = rStack;
        this.blockSide = side;
        this.player = player;
        this.world = world;

        this.x = x;
        this.y = y;
        this.z = z;

        block = world.getBlockId(x, y, z);
        meta = world.getBlockMetadata(x, y, z);
    }

    @Override
    public void run() {
        boolean doX, doY, doZ;
        doX = blockSide < 4;
        doY = blockSide > 1;
        doZ = blockSide < 2 || blockSide > 3;

        boolean[] blocks = new boolean[41];

        for (int i = 0; i < 41; i++) {
            int nX = x + (doX ? delta1[i] : 0);
            int nY = y + (doY ? (doX ? delta2[i] : delta1[i]) : 0);
            int nZ = z + (doZ ? delta2[i] : 0);

            if (req1[i] == -1 || blocks[req1[i]] == true || blocks[req2[i]] == true) {
                int tID = world.getBlockId(nX, nY, nZ), tMeta = world.getBlockMetadata(nX, nY, nZ);
                if (ReplaceUtils.canReplace(player, nX, nY, nZ, world, rStack) && tID == block && tMeta == meta && !world.isBlockFullCube(nX + (blockSide == 5 ? 1 : (blockSide == 4 ? -1 : 0)), nY + (blockSide == 1 ? 1 : (blockSide == 0 ? -1 : 0)), nZ + (blockSide == 3 ? 1 : (blockSide == 2 ? -1 : 0)))) {
                    blocks[i] = true;
                    ReplaceUtils.replaceBlockAt(player, nX, nY, nZ, world, rStack, ModBlockReplacer.energyCost, blockSide);
                    try {
                        Thread.sleep(25);
                    } catch (Exception e) {
                    }
                }
            }
        }
    }

    /*
     * @Override public void run() { boolean doX, doY, doZ; doX = blockSide < 4;
     * doY = blockSide > 1; doZ = blockSide < 2 || blockSide > 3;
     * 
     * replaceRecursive(x, y, z, doX, doY, doZ, 0); }
     */

    /*
     * private void replaceRecursive(int x, int y, int z, boolean doX, boolean
     * doY, boolean doZ, int recLevel) { int blockid = world.getBlockId(x, y,
     * z); int meta = world.getBlockMetadata(x, y, z);
     * 
     * if (blockid == blockToReplace.blockID && meta == btrMeta &&
     * ReplaceUtils.replaceBlockAt(player, x, y, z, world, rStack,
     * redstoneFluxPerBlock, blockSide)) { if (recLevel < radius) { if (doX) {
     * replaceRecursive(x - 1, y, z, true, doY, doZ, recLevel + 1);
     * replaceRecursive(x + 1, y, z, true, doY, doZ, recLevel + 1); } if (doY) {
     * replaceRecursive(x, y - 1, z, doX, true, doZ, recLevel + 1);
     * replaceRecursive(x, y + 1, z, doX, true, doZ, recLevel + 1); } if (doZ) {
     * replaceRecursive(x, y, z - 1, doX, doY, true, recLevel + 1);
     * replaceRecursive(x, y, z + 1, doX, doY, true, recLevel + 1); } } } }
     */
}
